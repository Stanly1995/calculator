package com.medvedev.calculator;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        String ERROR_MESSAGE = "Error";
        System.out.println("Calculator");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String str = scanner.nextLine();
            if (!str.equals("exit")) {
                try {
                    if (str.contains("+")) {
                        Plus plus = new Plus(str);
                        System.out.println(plus.printElements(plus.getElements()));
                    } else if (str.contains("-")) {
                        Minus minus = new Minus(str);
                        System.out.println(minus.printElements(minus.getElements()));
                    } else if (str.contains("*")) {
                        Multiply multiply = new Multiply(str);
                        System.out.println(multiply.printElements(multiply.getElements()));
                    } else if (str.contains("/")) {
                        Division division = new Division(str);
                        System.out.println(division.printElements(division.getElements()));
                    } else if (str.contains("^")) {
                        Exponent exponent = new Exponent(str);
                        System.out.println(exponent.printElements(exponent.getElements()));
                    } else
                        System.out.println(str);
                } catch (NumberFormatException e) {
                    System.out.println(ERROR_MESSAGE);
                }

                catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println(ERROR_MESSAGE);
                }
            }
            else
                break;
        }

    }

    public static abstract class MathOperation {
        public String str;
        public String newStr;
        public String newStr1;

        MathOperation(String str){
            this.str=str;
        }

        public String[] getElements(){
            newStr = str.replace(" ", "");
            newStr1 = newStr.replace("+", "-");
            newStr1 = newStr1.replace("*", "-");
            newStr1 = newStr1.replace("/", "-");
            newStr1 = newStr1.replace("^", "-");
            String[] newStrParts = newStr1.split("-");
            return newStrParts;

        }

        abstract Double printElements(String[] newStrParts);

    }

    public static class Plus extends MathOperation {
        Plus(String str) {
            super(str);
        }

        @Override
        public Double printElements(String[] newStrParts){
            return((Double.valueOf(newStrParts[0])) + (Double.valueOf(newStrParts[1])));
        }

    }

    public static class Minus extends MathOperation {
        Minus(String str) {
            super(str);
        }

        @Override
        public Double printElements(String[] newStrParts){
            return((Double.valueOf(newStrParts[0])) - (Double.valueOf(newStrParts[1])));
        }

    }

    public static class Multiply extends MathOperation {
        Multiply(String str) {
            super(str);
        }

        @Override
        public Double printElements(String[] newStrParts){
            return((Double.valueOf(newStrParts[0])) * (Double.valueOf(newStrParts[1])));
        }

    }

    public static class Division extends MathOperation {
        Division(String str) {
            super(str);
        }

        @Override
        public Double printElements(String[] newStrParts){
            return((Double.valueOf(newStrParts[0])) / (Double.valueOf(newStrParts[1])));
        }

    }

    public static class Exponent extends MathOperation {
        Exponent(String str) {
            super(str);
        }

        @Override
        public Double printElements(String[] newStrParts){
            return (Math.pow((Double.valueOf(newStrParts[0])),(Double.valueOf(newStrParts[1]))));
        }

    }



}